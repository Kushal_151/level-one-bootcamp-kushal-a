#include <math.h>
#include<stdlib.h>
#include<stdio.h>
struct Fraction
{

int Numerator;
int Denominator;

};

void inputFractions(struct Fraction *frac1,struct Fraction *frac2);
float computeSum(struct Fraction *frac1,struct Fraction *frac2);
void DisplaySum(struct Fraction *frac1,struct Fraction *frac2,float sum);
int checkDenominator(int den);


int main()
{
    float sum = 0.0;

    struct Fraction *frac1,*frac2;
    frac1 = (struct Fraction*)malloc(sizeof(struct Fraction));
    frac2 = (struct Fraction*)malloc(sizeof(struct Fraction));

    inputFractions(frac1,frac2);

    sum=computeSum(frac1,frac2);

    DisplaySum(frac1,frac2,sum);

    free(frac1);
    free(frac2);

    return 0;
}


void inputFractions(struct Fraction *frac1,struct Fraction *frac2)
{
  int num1, num2,den1,den2,den;

  
  printf("\nEnter the Numerator of the First Fraction :");
  scanf("%d",&num1);
  printf("Enter the Denominator of the First Fraction :");
  scanf("%d",&den1);

  den = checkDenominator(den1);

  frac1->Numerator = num1;
  frac1->Denominator = den;

  
  printf("\nEnter the Numerator of the Second Fraction :");
  scanf("%d",&num2);
  printf("Enter the Denominator of the second Fraction :");
  scanf("%d",&den2);

  den = checkDenominator(den2);

  frac2->Numerator = num2;
  frac2->Denominator = den;
}
  
int checkDenominator(int den)
{
      if(den != 0 )
     return den;
      if( den == 0)
   { 
     int Valid = 0;
        while(!Valid)
     {   
      
       printf("\nError: DIVIDING by zero gets infinity ..  re-enter valid value\n");
       printf("Enter 1 to provide a valid value , If you want to exit press any key  \n");
    int ch;
       scanf("%d",&ch);
       switch(ch)
       {
           case 0: printf("EXITING..!!\n");
                 exit(0);
           default : printf("\nEnter the valid Denominator  :");
                      scanf("%d",&den);
     if(checkDenominator(den))
          {
        Valid = 1;
        break;
      }
       }
    
       }
   } 
}

float computeSum(struct Fraction *frac1,struct Fraction *frac2)
{
    
float sum = (float) ((float)frac1->Numerator * (float)frac2->Denominator + (float) frac2->Numerator * (float) frac1->Denominator) /( (float) frac1->Denominator * (float) frac2->Denominator);
return sum;
}

void DisplaySum(struct Fraction *frac1,struct Fraction *frac2,float sum)
{
 printf("\nThe sum of the Fractions  is  = %f ", frac1->Numerator,frac1->Denominator, frac2->Numerator, frac2->Denominator , sum);
 return;
}