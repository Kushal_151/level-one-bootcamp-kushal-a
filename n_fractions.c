#include <stdio.h>
#include <math.h>
#include<stdlib.h>

struct Fraction
{

int Numerator;
int Denominator;

};

void inputFractions(struct Fraction* frac1,float *sum);
float computeSum(struct Fraction *frac1);
void DisplaySum(float *sum, int n);
int checkDenominator(int den);


int main()
{
    float *sum = NULL;
    int n;

    struct Fraction* frac1;
    frac1 = (struct Fraction*)malloc(sizeof(struct Fraction));
    sum = (float*)malloc(sizeof(float));

    printf("Enter the Limit of fractions : ");
    scanf("%d",&n);

    for(int i=0;i<n;i++)
    {
        inputFractions(frac1, sum);
    }

    DisplaySum(sum,n);
    return 0;
}


void inputFractions(struct Fraction* frac1,float *sum)
{
  int num,den;
  printf("Enter the Numerator :");
  scanf("%d",&num);
  printf("Enter the Denominator :");
  scanf("%d",&den);

  den = checkDenominator(den);
 
  frac1->Numerator = num;
  frac1->Denominator = den;

  *sum = *sum + computeSum(frac1);
 }
  
int checkDenominator(int den)
{
      if(den != 0 )
     return den;
      if( den == 0)
   { 
     int Valid = 0;
        while(!Valid)
     {   
      
       printf("\nError: DIVIDING By zero gets infinity..  re-enter valid value\n");
       printf("Enter 1 to provide a valid value , If you want to exit press any key \n");
    int ch;
       scanf("%d",&ch);
       switch(ch)
       {
           case 0: printf("EXITING..!!\n");
                 exit(0);
           default : printf("\nEnter the valid Denominator of the Fraction :");
                    scanf("%d",&den);
     if(checkDenominator(den))
                {
        Valid = 1;
        break;
      }
       }
    
      }
   } 
}

float computeSum(struct Fraction* frac1)
{
  float temp=(float) ((float)frac1->Numerator/(float)frac1->Denominator);
  return temp;
}

void DisplaySum(float *sum, int n)
{
 printf("\nThe sum of the %d Fractions is : %f ",n,*sum);
 return;
}